ThisBuild / name := "story-sudanese-cli"
ThisBuild / version := "0.1.0"
ThisBuild / scalaVersion := "2.12.8"

scalacOptions ++= Seq(
  "-deprecation",
  "-feature",
  "-unchecked",
  "-Xfatal-warnings",
  "-Xlint:_",
  "-Xverify",
)

lazy val root = (project in file("."))
  .dependsOn(engine)

lazy val engine = (project in file("engine"))
