/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese

import java.io._
import scala.io.{Source}
import ukkf.sudanese.ast.prog.{Program, ProgramBuilder}
import ukkf.sudanese.io.{Logger, StdInSource}

object Start {
  def main(args: Array[String]): Unit = try {
    if (args.length > 1) {
      println("More than one input file was specified!") // TODO i18n
    }

    val inputFile = if (args.length == 0) {
      StdInSource
    } else {
      Source.fromFile(args(0))
    }

    val log = if (args.length == 0) {
      Logger("<stdin>")
    } else {
      Logger(args(0))
    }

    val prog: Program = ProgramBuilder.buildProg(inputFile, log) match {
      case None =>
        println(s"Failed to build prog, log:")
        println(log.toString)
        sys.exit(1)
      case Some(p) =>
        p
    }

    if (log.toString.length > 0) {
      println(log.toString)
    }

    val baos = new ByteArrayOutputStream
    val oos = new ObjectOutputStream(baos)
    oos.writeObject(prog)
    oos.close()

    val bais = new ByteArrayInputStream(baos.toByteArray)
    val ois = new ObjectInputStream(bais)
    val prog2 = ois.readObject().asInstanceOf[Program]
    ois.close()

    val log2 = Logger("")

    new CliExecutor(prog2).execute(log2) match {
      case None =>
        println(s"An error occured while running the prog, log:")
        println(log2.toString)
        println(s"Old log:")
        println(log.toString)
        sys.exit(1)
      case Some(_) => {}
    }
  } catch {
    case e: Exception =>
      println(s"An uncaught Exception occured:")
      e.printStackTrace()
      sys.exit(1)
  }
}
