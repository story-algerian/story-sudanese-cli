/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese

import scala.io.AnsiColor._
import ukkf.sudanese.io.Color
import ukkf.sudanese.io.Color._

final object CliColor {
  def apply(col: Color): String = col match {
    case Black   => BLACK
    case Blue    => BLUE
    case Green   => GREEN
    case Cyan    => CYAN
    case Red     => RED
    case Magenta => MAGENTA
    case Brown   => YELLOW
    case Gray    => WHITE

    case DarkGray      => s"${BOLD}${BLACK}"
    case BrightBlue    => s"${BOLD}${BLUE}"
    case BrightGreen   => s"${BOLD}${GREEN}"
    case BrightCyan    => s"${BOLD}${CYAN}"
    case BrightRed     => s"${BOLD}${RED}"
    case BrightMagenta => s"${BOLD}${MAGENTA}"
    case Yellow        => s"${BOLD}${YELLOW}"
    case White         => s"${BOLD}${WHITE}"
  }

  def apply(col: Color, str: String): String = s"${apply(col)}${str}${RESET}"
}
