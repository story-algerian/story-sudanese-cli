/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese

import scala.io.StdIn
import scala.util.Random
import ukkf.sudanese.ast.block.CodeBlock
import ukkf.sudanese.ast.prog.Program
import ukkf.sudanese.exe.Executor
import ukkf.sudanese.io.err.{ChoiceQueueEmpty, RuntimeError}
import ukkf.sudanese.io.{Color, Logger}

final class CliExecutor(prog: Program) extends Executor {
  private class BadEndException extends Exception
  private class GoodEndException extends Exception

  var choices: List[(String, CodeBlock)] = Nil
  var color: Color = Color.White
  var _speed: Long = 0

  override def vars = prog.vars

  def speed = _speed
  override def speed_=(sp: BigInt) = _speed = sp.toLong

  def execute(log: Logger): Option[Unit] = {
    var currBlock: Option[CodeBlock] = Some(prog.initBlock)
    while (true) {
      try {
        currBlock match {
          case Some(b) =>
            b.execute(this, log) match {
              case None => return None
              case _ => {}
            }
            currBlock = None

          case None => return Some(())
        }
      } catch {
        case _: BadEndException =>
          println("You lost the game!")
          return Some(())
        case _: GoodEndException =>
          println("Congratulation!  You won!")
          return Some(())
      }
    }

    Some(())
  }

  override def addChoice(display: String, target: CodeBlock, log: Logger): Option[Unit] =
    Some(choices = (display, target) :: choices)

  override def badEnd(log: Logger): Option[Unit] = throw new BadEndException

  override def choose(log: Logger): Option[Unit] = choices match {
    case Nil =>
      log.err(-1, None, RuntimeError(ChoiceQueueEmpty))
      None

    case _ =>
      for (((display, _), i) <- choices.reverse.zipWithIndex) {
        println(s"${i + 1}. ${display}")
      }

      var in = -1
      while (in < 1 || in > choices.length) {
        print("> ")
        try {
          in = StdIn.readInt()
        } catch {
          case _: NumberFormatException => println("Please input a number")
        }
      }

      val (_, choice) = choices.reverse.drop(in - 1).head
      choices = Nil
      choice.execute(this, log)
  }

  override def chooseRandom(log: Logger): Option[Unit] = choices match {
    case Nil =>
      log.err(-1, None, RuntimeError(ChoiceQueueEmpty))
      None
      
    case _ =>
      choices = choices.drop(Random.nextInt(choices.length))
      val (_, choice) = choices.head
      choices = Nil
      choice.execute(this, log)
  }

  override def clearScreen(log: Logger): Option[Unit] = Some(()) // TODO
  override def goodEnd(log: Logger): Option[Unit] = throw new GoodEndException

  override def output(text: String, log: Logger): Option[Unit] = if (speed == 0) {
    Some(println(CliColor(color, text)))
  } else {
    print(CliColor(color))
    text foreach {
      case c =>
        print(c)
        Thread.sleep(speed)
    }

    Some(println(Console.RESET))
  }

  override def sleep(time: BigInt, log: Logger): Option[Unit] =
    Some(Thread.sleep(time.toLong))

  override def useColor(col: Color, log: Logger): Option[Unit] = Some(color = col)
  override def waitLine(log: Logger): Option[Unit] = {
    print("?")
    StdIn.readLine()
    Some(())
  }
}
